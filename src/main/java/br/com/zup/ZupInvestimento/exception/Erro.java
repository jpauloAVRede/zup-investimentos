package br.com.zup.ZupInvestimento.exception;

public class Erro {
    private String erro;

    public Erro(String erro) {
        this.erro = erro;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }
}
