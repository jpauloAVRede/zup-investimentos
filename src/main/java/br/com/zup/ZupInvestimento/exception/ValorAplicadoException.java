package br.com.zup.ZupInvestimento.exception;

public class ValorAplicadoException extends RuntimeException{
    public ValorAplicadoException(String message) {
        super(message);
    }
}
