package br.com.zup.ZupInvestimento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZupInvestimentoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZupInvestimentoApplication.class, args);
	}

}
