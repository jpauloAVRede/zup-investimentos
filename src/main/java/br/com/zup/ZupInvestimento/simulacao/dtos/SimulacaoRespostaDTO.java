package br.com.zup.ZupInvestimento.simulacao.dtos;

import java.time.LocalDate;

public class SimulacaoRespostaDTO {
    private double quantiaAplicada;
    private double rendimento;
    private LocalDate dataDeResgate;

    public SimulacaoRespostaDTO() {
    }

    public double getQuantiaAplicada() {
        return quantiaAplicada;
    }

    public void setQuantiaAplicada(double quantiaAplicada) {
        this.quantiaAplicada = quantiaAplicada;
    }

    public double getRendimento() {
        return rendimento;
    }

    public void setRendimento(double rendimento) {
        this.rendimento = rendimento;
    }

    public LocalDate getDataDeResgate() {
        return dataDeResgate;
    }

    public void setDataDeResgate(LocalDate dataDeResgate) {
        this.dataDeResgate = dataDeResgate;
    }
}
