package br.com.zup.ZupInvestimento.simulacao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "simulacoes")
public class Simulacao {
    @Id
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private String nome;
    private String telefone;
    @Column(nullable = false)
    private double quantiaAplicada;
    @Column(nullable = false)
    private int mesesAplicado;

    public Simulacao() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public double getQuantiaAplicada() {
        return quantiaAplicada;
    }

    public void setQuantiaAplicada(double quantiaAplicada) {
        this.quantiaAplicada = quantiaAplicada;
    }

    public int getMesesAplicado() {
        return mesesAplicado;
    }

    public void setMesesAplicado(int mesesAplicado) {
        this.mesesAplicado = mesesAplicado;
    }
}
