package br.com.zup.ZupInvestimento.simulacao;

import br.com.zup.ZupInvestimento.simulacao.dtos.CadastrarSimulacaoDTO;
import br.com.zup.ZupInvestimento.simulacao.dtos.SimulacaoRespostaDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacao")
public class SimulacaoController {
    @Autowired
    private SimulacaoService simulacaoService;
    @Autowired
    private ModelMapper modelMapper;

    @PutMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoRespostaDTO cadastrarSimulacao(@RequestBody @Valid CadastrarSimulacaoDTO cadastrarSimulacaoDTO){
        Simulacao simulacao = modelMapper.map(cadastrarSimulacaoDTO, Simulacao.class);
        SimulacaoRespostaDTO simulacaoRespostaDTO = new SimulacaoRespostaDTO();

        simulacaoRespostaDTO.setRendimento(this.simulacaoService.calcularRendaFixa(cadastrarSimulacaoDTO.getQuantiaAplicada(), cadastrarSimulacaoDTO.getMesesAplicado()));
        simulacaoRespostaDTO.setDataDeResgate(this.simulacaoService.calcularDataResgate(cadastrarSimulacaoDTO.getMesesAplicado()));
        simulacaoRespostaDTO.setQuantiaAplicada(cadastrarSimulacaoDTO.getQuantiaAplicada());

        this.simulacaoService.salvarSimulacaoDeInvestimento(simulacao);

        return simulacaoRespostaDTO;
    }

}
