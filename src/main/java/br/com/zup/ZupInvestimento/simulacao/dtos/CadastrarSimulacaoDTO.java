package br.com.zup.ZupInvestimento.simulacao.dtos;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class CadastrarSimulacaoDTO {
    @Size(min = 2, max = 20, message = "{validacao.nome}")
    private String nome;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.email.vazio}")
    private String email;
    @Min(value = 1, message = "{validacao.qtdAplicada}")
    private double quantiaAplicada;
    @Min(value = 1, message = "{validacao.mesesAplicado}")
    private int mesesAplicado;

    public CadastrarSimulacaoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getQuantiaAplicada() {
        return quantiaAplicada;
    }

    public void setQuantiaAplicada(double quantiaAplicada) {
        this.quantiaAplicada = quantiaAplicada;
    }

    public int getMesesAplicado() {
        return mesesAplicado;
    }

    public void setMesesAplicado(int mesesAplicado) {
        this.mesesAplicado = mesesAplicado;
    }
}
