package br.com.zup.ZupInvestimento.simulacao;

import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, String> {

}
