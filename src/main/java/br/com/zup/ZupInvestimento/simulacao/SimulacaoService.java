package br.com.zup.ZupInvestimento.simulacao;

import br.com.zup.ZupInvestimento.exception.ValorAplicadoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class SimulacaoService {
    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public void salvarSimulacaoDeInvestimento(Simulacao simulacao){
        this.simulacaoRepository.save(simulacao);
    }

    public double calcularRendaFixa(double valorAplicado, int qtdMeses){
        if (valorAplicado >= 1000){
            double valorRendaFixa = (valorAplicado * ((0.5/100) * qtdMeses));
            return valorRendaFixa;
        }
        throw new ValorAplicadoException("Valor aplicado deve ser maior que R$ 1000");
    }

    public LocalDate calcularDataResgate(int qtdMeses){
        return LocalDate.now().plusMonths(qtdMeses);
    }

}
