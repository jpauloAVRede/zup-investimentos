package br.com.zup.ZupInvestimento.simulacao;

import br.com.zup.ZupInvestimento.simulacao.dtos.CadastrarSimulacaoDTO;
import br.com.zup.ZupInvestimento.simulacao.dtos.SimulacaoRespostaDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;

@WebMvcTest(SimulacaoController.class)
public class SimulacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SimulacaoService simulacaoService;
    @MockBean
    private ModelMapper modelMapper;

    private Simulacao simulacao;
    private CadastrarSimulacaoDTO cadastrarSimulacaoDTO;
    private SimulacaoRespostaDTO simulacaoRespostaDTO;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp(){
        simulacao = new Simulacao();

        cadastrarSimulacaoDTO = new CadastrarSimulacaoDTO();
        cadastrarSimulacaoDTO.setQuantiaAplicada(1000.00);
        cadastrarSimulacaoDTO.setEmail("xablal@xablal");
        cadastrarSimulacaoDTO.setMesesAplicado(12);
        cadastrarSimulacaoDTO.setNome("joao");

        simulacaoRespostaDTO = new SimulacaoRespostaDTO();

        objectMapper = new ObjectMapper();
    }

    @Test
    public void testarMetodoCadastrarSimulacaoCaminhoPositivo() throws Exception {
        LocalDate dataTeste = LocalDate.now().plusMonths(12);
        this.simulacaoService.salvarSimulacaoDeInvestimento(Mockito.any(Simulacao.class));

        Mockito.when(this.simulacaoService.calcularRendaFixa(1000, 12)).thenReturn(60.00);
        Mockito.when(this.simulacaoService.calcularDataResgate(12)).thenReturn(dataTeste);
        Mockito.verify(simulacaoService, Mockito.times(1))
                .salvarSimulacaoDeInvestimento(modelMapper.map(cadastrarSimulacaoDTO, Simulacao.class));

        String json = objectMapper.writeValueAsString(cadastrarSimulacaoDTO);

        ResultActions resultadoDaRequisicao = mockMvc
                .perform(MockMvcRequestBuilders.put("/simulacao")
                        .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }



}
