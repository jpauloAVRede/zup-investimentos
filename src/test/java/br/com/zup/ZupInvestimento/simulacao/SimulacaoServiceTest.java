package br.com.zup.ZupInvestimento.simulacao;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;

@SpringBootTest
public class SimulacaoServiceTest {
    @Autowired
    private SimulacaoService simulacaoService;
    @MockBean
    private SimulacaoRepository simulacaoRepository;

    private Simulacao simulacao;

    @BeforeEach
    public void setUp(){
        simulacao = new Simulacao();
    }


    @Test
    public void testarMetodoSalvarSimulacaoCaminhoFeliz(){
        this.simulacaoService.salvarSimulacaoDeInvestimento(simulacao);
        Mockito.verify(simulacaoRepository, Mockito.times(1)).save(simulacao);
    }

    @Test
    public void testarMetodoCalcularRendaFixaCaminhoFeliz(){
        Assertions.assertEquals(60, this.simulacaoService.calcularRendaFixa(1000, 12));
    }

    @Test
    public void testarMetodoCalcularRendaFixaCaminhoNegativo(){
        RuntimeException exception = Assertions
                .assertThrows(RuntimeException.class, () -> {this.simulacaoService.calcularRendaFixa(10,12);});

        Assertions.assertTrue(exception.getMessage().equals("Valor aplicado deve ser maior que R$ 1000"));
    }

    @Test
    public void testarMetodoCalcularDataResgate(){
        LocalDate dataEsperada = LocalDate.now().plusMonths(12);
        Assertions.assertEquals(dataEsperada, this.simulacaoService.calcularDataResgate(12));
    }

}